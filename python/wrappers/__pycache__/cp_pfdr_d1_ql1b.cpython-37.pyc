B
    ig�^�:  �               @   s�   d dl Zd dlZd dlZej�ej�ej�ej�e	��d�� d dl
m
Z
 ddddddddddded	�ed
�d dddddfdd�ZdS )�    Nz../bin)�cp_pfdr_d1_ql1b_cpyg-C��6?�
   g      �?g{�G�z�?g        g     ��@g     @�@TFc       "      C   s0  t | �tjkr | jdkr | j}n(t |�tjkr@|jdkr@|j}ntd��|dkrXtd��t | �tjkr�| dkr~tjg |d�} ntd��t |�tjkr�t |�tkr�td��nt�|g|�}t |�tjks�|jd	kr�td
��t |�tjks�|jd	kr�td��t |�tjk�rLt |�tk�r td��n,|dk�r<tj|g|d�}ntjdg|d�}t |�tjk�r~|dk�rvtjg |d�}ntd��t |�tjk�r�t |�tk�r�td��n,|dk�r�tj|g|d�}ntjdg|d�}t |�tjk�r*t |�tk�r�td��n0|dk�rtj|g|d�}ntjtj g|d�}t |�tjk�r�t |�tk�rRtd��n.|dk�rntj|g|d�}ntjtjg|d�}|j	dk�r�|j
d dk�r�|j
d }nB|j
d dk�r�| jdk�r�| j}n|jdk�r�|j}n
|j
d }|j|d k�rtd�|d |j���xPtdddddddg| ||||||g�D ]&\}}|j|k�r8td�||����q8W | jd �svtd��|jd �s�td��|dk�r�|	d  }t|	�}	t|�}t|�}t|�}t|�}t|
�}
t|�}t|�}t|�}xHtd!d"d#d$d%g|||||g�D ]&\}}t |�tk�rtd&�|����qW t| |||||||||	|
||||||||||d'k|||�\}}}}} }!|d }|�r�|�r�|�r�||||| |!fS |�r�|�r�||||| fS |�r�|�r�|||||!fS |�r�|�r�|||| |!fS |�r�||||fS |�r|||| fS |�r"||||!fS |||fS dS )(a�  
    Comp, rX, cp_it, Obj, Time, Dif = cp_pfdr_d1_ql1b(
            Y | AtY, A | AtA, first_edge, adj_vertices, edge_weights=None,
            Yl1=None, l1_weights=None, low_bnd=None, upp_bnd=None,
            cp_dif_tol=1e-5, cp_it_max=10, pfdr_rho=1.0, pfdr_cond_min=1e-2,
            pfdr_dif_rcd=0.0, pfdr_dif_tol=1e-3*cp_dif_tol,
            pfdr_it_max=int(1e4), verbose=int(1e3), AtA_if_square=True,
            max_num_threads=0, balance_parallel_split=True, compute_Obj=False,
            compute_Time=False, compute_Dif=False)

    Cut-pursuit algorithm with d1 (total variation) penalization, with a 
    quadratic functional, l1 penalization and box constraints:

    minimize functional over a graph G = (V, E)

        F(x) = 1/2 ||y - A x||^2 + ||x||_d1 + ||yl1 - x||_l1 + i_[m,M](x)

    where y in R^N, x in R^V, A in R^{N-by-|V|}
          ||x||_d1 = sum_{uv in E} w_d1_uv |x_u - x_v|,
          ||x||_l1 = sum_{v  in V} w_l1_v |x_v|,
          and the convex indicator
          i_[m,M] = infinity if it exists v in V such that x_v < m_v or 
          x_v > M_v
                  = 0 otherwise;

    using cut-pursuit approach with preconditioned forward-Douglas-Rachford 
    splitting algorithm.

    It is easy to introduce a SDP metric weighting the squared l2-norm
    between y and A x. Indeed, if M is the matrix of such a SDP metric,
    ||y - A x||_M^2 = ||Dy - D A x||^2, with D = M^(1/2).
    Thus, it is sufficient to call the method with Y <- Dy, and A <- D A.
    Moreover, when A is the identity and M is diagonal (weighted square l2 
    distance between x and y), one should call on the precomposed version 
    (see below) with Y <- DDy = My and A <- D2 = M.

    INPUTS: real numeric type is either float32 or float64, not both;
            indices numeric type is uint32.

    NOTA: by default, components are identified using uint16 identifiers; 
    this can be easily changed in the wrapper source if more than 65535
    components are expected (recompilation is necessary)

    Y - observations, (real) array of length N (direct matricial case) or
                             array of length V (left-premult. by A^t), or
                             empty matrix (for all zeros)
    A - matrix, (real) N-by-V array (direct matricial case), or
                       V-by-V array (premultiplied to the left by A^t), or
                       V-by-1 array (_square_ diagonal of A^t A = A^2), or
                       nonzero scalar (for identity matrix), or
                       zero scalar (for no quadratic part);
        for an arbitrary scalar matrix, use identity and scale observations
        and penalizations accordingly
        if N = V in a direct matricial case, the last argument 'AtA_if_square'
        must be set to false
    first_edge, adj_vertices - graph forward-star representation:
        edges are numeroted (C-style indexing) so that all vertices originating
        from a same vertex are consecutive;
        for each vertex, 'first_edge' indicates the first edge starting 
            from the vertex (or, if there are none, starting from the next 
            vertex); (uint32) array of length V + 1, the last value is the
            total number of edges;
        for each edge, 'adj_vertices' indicates its ending vertex, (uint32)
            array of length E
    edge_weights - (real) array of length E or a scalar for homogeneous weights
    Yl1 - offset for l1 penalty, (real) array of length V
    l1_weights - (real) array of length V or scalar for homogeneous weights
    low_bnd - (real) array of length V or scalar
    upp_bnd - (real) array of length V or scalar
    cp_dif_tol - stopping criterion on iterate evolution; algorithm stops if
        relative changes (in Euclidean norm) is less than dif_tol;
        1e-4 is a typical value; a lower one can give better precision
        but with longer computational time and more final components
    cp_it_max - maximum number of iterations (graph cut and subproblem)
        10 cuts solve accurately most problems
    pfdr_rho - relaxation parameter, 0 < rho < 2
        1 is a conservative value; 1.5 often speeds up convergence
    pfdr_cond_min - stability of preconditioning; 0 < cond_min < 1;
        corresponds roughly the minimum ratio to the maximum descent metric;
        1e-2 is typical; a smaller value might enhance preconditioning
    pfdr_dif_rcd - reconditioning criterion on iterate evolution;
        a reconditioning is performed if relative changes of the iterate drops
        below dif_rcd; WARNING: reconditioning might temporarily draw minimizer
        away from the solution set and give bad subproblem solutions
    pfdr_dif_tol - stopping criterion on iterate evolution; algorithm stops if
        relative changes (in Euclidean norm) is less than dif_tol
        1e-3*cp_dif_tol is a conservative value
    pfdr_it_max - maximum number of iterations 1e4 iterations provides enough
        precision for most subproblems
    verbose - if nonzero, display information on the progress, every 'verbose'
        PFDR iterations
    max_num_threads - if greater than zero, set the maximum number of threads
        used for parallelization with OpenMP
    balance_parallel_split - if true, the parallel workload of the split step 
        is balanced; WARNING: this might trade off speed against optimality
    AtA_if_square - if A is square, set this to false for direct matricial case
    compute_Obj - compute the objective functional along iterations 
    compute_Time - monitor elapsing time along iterations
    compute_Dif - compute relative evolution along iterations 

    OUTPUTS: Obj, Time, Dif are optional, set parameters compute_Obj,
        compute_Time, compute_Dif to True to request them

    Comp - assignement of each vertex to a component, (uint16) array of
        length V
    rX - values of each component of the minimizer, (real) array of length rV;
        the actual minimizer is then reconstructed as X = rX[Comp];
    cp_it - actual number of cut-pursuit iterations performed
    Obj - if requested, the values of the objective functional along iterations
          array of length cp_it + 1; in the precomputed A^t A version, a
        constant 1/2||Y||^2 in the quadratic part is omited
    Time - if requested, the elapsed time along iterations
           array of length cp_it + 1
    Dif  - if requested, the iterate evolution along iterations
           array of length cp_it

    Parallel implementation with OpenMP API.

    H. Raguet and L. Landrieu, Cut-Pursuit Algorithm for Regularizing Nonsmooth
    Functionals with Graph Total Variation, International Conference on Machine
    Learning, PMLR, 2018, 80, 4244-4253

    H. Raguet, A Note on the Forward-Douglas--Rachford Splitting for Monotone 
    Inclusion and Convex Optimization Optimization Letters, 2018, 1-24

    Baudoin Camille 2019
    r   zvCut-pursuit d1 quadratic l1 bounds: at least one of arguments 'Y' or 'Yl1' must be provided as a nonempty numpy array.)�float32�float64z`Cut-pursuit d1 quadratic l1 bounds: currently, the real numeric type must be float32 or float64.N)�dtypezGCut-pursuit d1 quadratic l1 bounds: argument 'Y' must be a numpy array.zSCut-pursuit d1 quadratic l1 bounds: argument 'A' must be a scalar or a numpy array.�uint32z_Cut-pursuit d1 quadratic l1 bounds: argument 'first_edge' must be a numpy array of type uint32.zaCut-pursuit d1 quadratic l1 bounds: argument 'adj_vertices' must be a numpy array of type uint32.z^Cut-pursuit d1 quadratic l1 bounds: argument 'edge_weights' must be a scalar or a numpy array.g      �?zICut-pursuit d1 quadratic l1 bounds: argument 'Yl1' must be a numpy array.z\Cut-pursuit d1 quadratic l1 bounds: argument 'l1_weights' must be a scalar or a numpy array.g        zYCut-pursuit d1 quadratic l1 bounds: argument 'low_bnd' must be a scalar or a numpy array.zYCut-pursuit d1 quadratic l1 bounds: argument 'upp_bnd' must be a scalar or a numpy array.�   zsCut-pursuit d1 quadratic l1 bounds: argument 'first_edge' should contain |V + 1| = {0} elements, but {1} are given.�Y�A�edge_weights�Yl1�
l1_weights�low_bnd�upp_bndz$argument '{0}' must be of type '{1}'�F_CONTIGUOUSzECut-pursuit d1 quadratic l1 bounds: argument 'Y' must be F_CONTIGUOUSzECut-pursuit d1 quadratic l1 bounds: argument 'A' must be F_CONTIGUOUSg����MbP?�AtA_if_square�balance_parallel_split�compute_Obj�compute_Time�compute_DifzBCut-pursuit d1 quadratic l1 bounds: argument '{0}' must be booleanr   )�type�np�ndarray�sizer   �	TypeError�array�list�inf�ndim�shape�
ValueError�format�zip�flags�float�int�boolr   )"r	   r
   �
first_edge�adj_verticesr   r   r   r   r   Z
cp_dif_tol�	cp_it_max�pfdr_rhoZpfdr_cond_minZpfdr_dif_rcdZpfdr_dif_tolZpfdr_it_max�verboseZmax_num_threadsr   r   r   r   r   �real_t�V�nameZar_argsZb_args�Comp�rX�itZObjZTimeZDif� r2   �L/home/latroche/Codes/parallel-cut-pursuit/python/wrappers/cp_pfdr_d1_ql1b.py�cp_pfdr_d1_ql1b
   s�     













r4   )�numpyr   �os�sys�path�append�join�realpath�dirname�__file__r   r%   r4   r2   r2   r2   r3   �<module>   s   